from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings


from stock import views as stock_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', stock_views. ProductListView.as_view(), name='product_list'),
    path('fora_de_estoque', stock_views.OutOfStockListView.as_view(), name='out_of_stock'),
    path('adicionar_estoque/<int:pk>', stock_views.AddStockView.as_view(), name='add_stock'),
    path('tirar_estoque/<int:pk>', stock_views.DecreaseStockView.as_view(), name='decrease_stock')
    ]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
