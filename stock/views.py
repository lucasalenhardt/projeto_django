from django.views.generic import ListView, DetailView
from stock.models import Product
from django.shortcuts import redirect

class ProductListView(ListView):
    model = Product



class OutOfStockListView(ListView):
    model = Product

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantity=0)



class AddStockView(DetailView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantity = self.request.POST['quantity']
        product.quantity += int(quantity)
        product.save()
        return redirect('product_list')


class DecreaseStockView(DetailView):
    model = Product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantity = self.request.POST['quantity']
        product.quantity -= int(quantity)
        product.save()
        return redirect('product_list')